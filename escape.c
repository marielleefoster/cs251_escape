/* Implementation of the escape and unescape functions 

By Marielle Foster*/

#include <stdlib.h>
#include "escape.h"
#include <stdio.h>
#include <string.h>


int special_count(char *unescaped){

  int i = 0;
  int k = 0;
  while(unescaped[k] != '\0'){
    switch (unescaped[k]) {
      case '\n':     
        i++;
          
        break;

      case '\t':
        i++;
        break;

      case '\\':
        i++;
        break;

      case '\'':
        i++;
        break;

      case '\"':
        i++;
        break;

      default:
        
        break;
    }
    k++;    
  }
  return i;
}

//for unescaped, counts only backslashes
int special_count_back(char *unescaped){

  int i = 0;
  int k = 0;
  while(unescaped[k] != '\0'){
    if(unescaped[k] == '\\'){
      if(unescaped[k+1] != '\\'){
        i++;
      }
    }
    k++;  
  }
  return i;
}


/* This method replaces special characters in the string with their
   escaped two-character versions. */
char *escape(char *unescaped) {

  int new_arr_size = special_count(unescaped) + strlen(unescaped);

  //printf("New array size: %i\n", new_arr_size);
  char *escaped;
  escaped = (char*)malloc(new_arr_size*sizeof(char));
  int i = 0;
  int k = 0;
  while(unescaped[i] != '\0'){
    switch (unescaped[i]) {
      case '\n':     
        //printf("new line\n");  
        escaped[k] = '\\';
        k++;
        escaped[k] = 'n';     
        break;

      case '\t':
        //printf("tab\n");
        escaped[k] = '\\';
        k++;
        escaped[k] = 't';        
        break;

      case '\\':
        //printf("backslash\n");
        escaped[k] = '\\';
        k++;
        escaped[k] = '\\';
        break;

      case '\'':
        //printf("single quote\n");
        escaped[k] = '\\';
        k++;
        escaped[k] = '\'';  
        break;

      case '\"':
        //printf("double quote\n");
        escaped[k] = '\\';
        k++;
        escaped[k] = '\"'; 
        break;

      default:
        //printf("%c\n", unescaped[i]);
        escaped[k] = unescaped[i];
        break;
    }
    i++;
    k++;
  }
  return escaped;
}


/* This method replaces escaped characters with their one-character
   equivalents. */
char *unescape(char *escaped) {
  int new_arr_size = strlen(escaped)-special_count_back(escaped);

  //printf("New array size: %i\n", new_arr_size);
  char *unescaped;
  unescaped = (char*)malloc(new_arr_size*sizeof(char));
  int i = 0;
  int k = 0;
  while(escaped[i] != '\0'){
    if(escaped[i] == '\\'){
      i++;
      switch (escaped[i]) {      
        case 'n':
          //printf("new line\n");
          unescaped[k] = '\n';
          break;
        
        case 't':
          //printf("tab\n");
          unescaped[k] = '\t';   
          break;

        case '\\':
          //printf("backslash\n");
          unescaped[k] = '\\';
          break;

        case '\'':
          //printf("single quote\n");
          unescaped[k] = '\'';
          break;

        case '\"':
          //printf("double quote\n");
          unescaped[k] = '\"';
          break;
      }      
    }
    else {
      unescaped[k] = escaped[i];
    }
    i++;
    k++;
       
  }   
  return unescaped;

}