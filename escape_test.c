/* Testing the escape and unescape functions

By Marielle Foster */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "escape.h"

int main() {
  char *hello = "Hello\n\n\n\\world";

  /* Should print:

Hello\n\n\n\\world

  */
  char *hello_escaped = escape(hello);
  printf("%s\n", hello_escaped);

  char *marielle = "Marielle\nElizabeth\tSorum\'Foster\\";

  char *marielle_escaped = escape(marielle);
  printf("%s\n", marielle);
  printf("%s\n", marielle_escaped);

  /* Should print:

Hello


\world

  */
  // char *marielle_escaped_unescaped = unescape(marielle_escaped);
  // printf("%s\n", marielle_escaped_unescaped);
  char *hello_escaped_unescaped = unescape(hello_escaped);
  printf("%s\n", hello_escaped_unescaped);
  
  /* check "round-trip" */
  assert(strcmp(hello, hello_escaped_unescaped) == 0);
  free(marielle_escaped);
  free(hello_escaped);
  // free(marielle_escaped_unescaped);
  free(hello_escaped_unescaped);
}
